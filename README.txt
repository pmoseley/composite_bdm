--------------AUTHOR-----------------------------------
Philip Moseley
PhD Candidate, Belytschko Research Group
Northwestern University
Philip.Moseley@u.northwestern.edu

--------------LICENSE----------------------------------
This code is licensed under the Academic Free License v3.0. You may modify and distribute the code as you please, as long as you maintain the attribution notices.

--------------USAGE------------------------------------
Run bdm.m
Input can be defined in bdm.m, or given as an input structure.

--------------REFERENCES-------------------------------
Xiao S, Belytschko T. A bridging domain method for coupling continua with molecular dynamics.
Computer Methods in Applied Mechanics and Engineering 2004; 193:1645–1669, doi:10.1016/j.cma.2003.
12.053.

Zhang S, Khare R, Lu Q, Belytschko T. A bridging domain and strain computation method for coupled
atomistic-continuum modelling of solids. International Journal for Numerical Methods in Engineering
2007.

Xu M, Gracie R, Belytschko T. A continuum-to-atomistic bridging domain method for composite lattices.
International Journal for Numerical Methods in Engineering 2010; 81(13):1635–1658.

