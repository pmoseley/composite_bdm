ui.dt   = 0.0005;           % Timestep (ps).
ui.t_final = 0.500;         % Total simulation time.
ui.io_step = 30;            % Timesteps between plots.
ui.print_times = ...        % List of times to print.
    [0,ui.t_final/2.0,ui.t_final];
ui.output_file = 'high_frequency';  % Name of output files.

ui.num_atoms = 200;         % Number of atoms.
ui.num_nodes = 20;          % Number of nodes.
ui.bdm_overlap = 30.0;      % Overlapping domain length (Angstroms).

ui.dx_node =  10.0;         % Nodal spacing (Angstroms).
ui.potential.sigma = 1.1;   % Angstroms.
ui.potential.epsilon = 0.2; % eV.
ui.atom_mass =  1.036e-4;   % Mass per atom. (eV*ps^2/A^2)
% ui.BDM_consistent = true;   % Consistent or diagonalized constraint matrix.
ui.BDM_consistent = false;  % Consistent or diagonalized constraint matrix.

% Initial conditions.
ui.ic_region = 10.0;        % Size of domain IC is applied to (Angstroms).
ui.initial_conditions = @(x) 0.01*cos(x*pi/2.0/ui.ic_region);

ui.BC.fix.right  =  false;

addpath(genpath('../'));
bdm(ui)

