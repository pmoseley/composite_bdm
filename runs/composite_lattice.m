function composite_lattice()
    ui.N    = 3;
    ui.dt   = 0.0005;           % Timestep (ps).
    ui.t_final = 2;             % Total simulation time.
    ui.io_step = 30;            % Timesteps between plots.
    ui.print_times = ...        % List of times to print.
        [0,ui.t_final/2.0,ui.t_final];
    ui.output_file = 'composite_lattice';  % Name of output files.
            
    ui.num_atoms = 101;         % Number of atoms.
    ui.num_nodes = 15;          % Number of nodes.
    ui.bdm_overlap = 50.0;      % Overlapping domain length (Angstroms).

    ui.dx_node =  10.0;         % Nodal spacing (Angstroms).
    ui.potential.sigma = [1.1, 1.1];    % Angstroms.
    ui.potential.epsilon = [0.1, 0.05]; % eV.
    ui.atom_mass =  [1.036e-4  1.036e-4];  % Mass per atom. (eV*ps^2/A^2)
    ui.ConstraintAtomType = 1;   % Constrain is applied on atom type 1

    ui.BDM_consistent = false;   % Consistent or diagonalized constraint matrix.

    % Initial conditions.
    % Set Boundary Condition
    T =  0.5;                   % set the cycle time  
    ui.magnitude     = 0.005;
    ui.LeftBC        = true;
    ui.RightBC       = false;
    ui.LeftBoundary  = @(t) OneCycleSinFunc(ui.magnitude,t,T);
    ui.RightBoundary = @(t) 0;   % can be written in class. constructor.
    addpath(genpath('../'));
    bdm(ui)
end

function [LeftBC y]=OneCycleSinFunc(magnitude,t,T)
    if (t<T/2)
        LeftBC = true;
        y=magnitude*sin(t*2*pi/T);
    else
        y=0;
        LeftBC = false;
    end
end

