function small_bd()
    ui.N    = 3;
    ui.dt   = 0.0005;           % Timestep (ps).
    ui.t_final = 2;         % Total simulation time.
    ui.io_step = 30;            % Timesteps between plots.
    ui.print_times = ...        % List of times to print.
        linspace(0,ui.t_final,20);
    %     [0,ui.t_final/2.0,ui.t_final];
    ui.output_file = 'small_bd';  % Name of output files.
            
    ui.num_atoms = 150;         	% Number of atoms.
    ui.num_nodes = 30;          % Number of nodes.
    ui.bdm_overlap = 0.1;      % Overlapping domain length (Angstroms).

    ui.dx_node =  10.0;         % Nodal spacing (Angstroms).
    ui.potential.sigma = 1.1;   % Angstroms.
    ui.potential.epsilon = 0.05; % eV.
    ui.atom_mass =  1.036e-4;   % Mass per atom. (eV*ps^2/A^2)
    ui.BDM_consistent = false;   % Consistent or diagonalized constraint matrix.

    % Initial conditions.
    ui.ic_region = 45.0;        % Size of domain IC is applied to (Angstroms).
    ui.initial_conditions = @(x) 0.01*cos(x*pi/2.0/ui.ic_region);
    % Set Boundary Condition
    T =  1;                   % set the cycle time  
    ui.magnitude     = 0.005;
    ui.LeftBC        = true;
    ui.RightBC       = false;
    ui.LeftBoundary  = @(t) OneCycleSinFunc(ui.magnitude,t,T);
    ui.RightBoundary = @(t) 0;   % can be written in class. constructor.
    addpath(genpath('../'));
    bdm(ui)
end

function [LeftBC y]=OneCycleSinFunc(magnitude,t,T)
    if (t<T/2)
        LeftBC = true;
        y=magnitude*sin(t*2*pi/T);
    else
        y=0;
        LeftBC = false;
    end
end

