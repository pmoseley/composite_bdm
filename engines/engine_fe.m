classdef engine_fe
    properties
        dx
        mass
        X
        x
        v
        a0
        a1
        KE
        PE
        e_total
    end
    properties (Hidden)
        MD
        rho
        nn
        ne
        conn
    end
    methods
        % Constructor.
        function FE = engine_fe(MD, nn, dx, x0, time)
            FE.MD = MD;
            FE.rho = MD.density();
            FE.dx = dx;
            FE.nn = nn;
            FE.ne = nn-1;
            FE.X  = [x0:dx:x0 + FE.ne*FE.dx];
            FE.x  = zeros(nn,1);
            FE.v  = zeros(nn,1);
            FE.a0 = zeros(nn,1);
            FE.a1 = zeros(nn,1);
            FE.conn = [1:FE.ne;2:nn];
            FE.mass = FE.calc_mass_matrix();
            FE.KE = zeros(length(time),1);
            FE.PE = zeros(length(time),1);
            FE.e_total = zeros(length(time),1);
        end

        % Return the number of atoms.
        function nn = size(FE)
            nn = FE.nn;
        end
        % Return the length of the domain.
        function len = length(FE)
            len = FE.ne*FE.dx;
        end

        % Calculate the FE forces.
        function f = forces(FE,alpha,RightBC)
            [gp,wgp] = FE.gauss2();
            J = FE.dx/2;
            f.int = zeros(FE.size(),1);
            f.ext = zeros(FE.size(),1);
            % Calculate forces on each node.
            for e = FE.conn
                fe = zeros(2,1);
                ue = FE.x(e);
                for it = 1:length(gp)
                    xita = gp(it);
                    w  = wgp(it);
                    dN = FE.shape_function_gradient(xita)/J; % dN/dX J = le/2
                    F = 1.0 + dN'*ue; % F is a number in this 1D problem
                    P = FE.MD.calculate_PK1(F);
                    fe = fe + alpha(e).*dN*w*P*J;
                end
                f.int(e)=f.int(e)+fe;
            end
            if(RightBC)
                f.int(FE.nn)  =   0;
            end
        end

    end
    methods (Access = private)
        % Calculate the diagonalized mass matrix.
        function m = calc_mass_matrix(FE)
            m = zeros(FE.nn,1);
            [gp,wgp] = FE.gauss2();% 2 gauss integration points in one element
            for e = FE.conn
                Je = FE.dx/2;        
                me = zeros(2,2);
                for it = 1:length(gp)
                    % for two gauss integration points
                    Ne = FE.shape_functions(gp(it));
                    w  = wgp(it);
                    me = me + w * FE.rho * Ne'*Je*Ne;   
                end
                me = sum(me,2);
                m(e)=m(e)+me;
            end
        end

    end
    methods (Static)
        % Linear shape function for 1d on parent configuration.
        function N =shape_functions(xita)
            N1=(1-xita)/2;
            N2=(1+xita)/2;
            N=[N1,N2];
        end

        function [dN_xita]=shape_function_gradient(xita)
            dN_xita=0.5*[-1;1]; % for now doesn't need to use xitas
        end

        % Gauss points for 1d problem.
        function [gp,wgp] = gauss2()
            gp  = 1/sqrt(3)*[-1,1];
            wgp = [1,1];
        end
    end
end

