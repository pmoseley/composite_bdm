classdef engine_bdm
    properties
        weights
        inv_mass
        atoms
        nodes
    end
    properties (Hidden)
        real_l_bri
        consistent = false
    end
    methods
        % Constructor.
        function BDM = engine_bdm(MD,FE,bdm_overlap,BDM_consistent,ConstraintAtomType)
            LeftBoundary  = MD.X(end)-bdm_overlap;
            RightBoundary = FE.X(1)+bdm_overlap;
            for i=1:MD.size()
                if(MD.X(i)>=LeftBoundary)
                    
                    % assign constrains to different types of atoms
                    if (ConstraintAtomType == 0)
                        BDM.atoms = [i:MD.size()];
                    else
                        atomSet1 = [i  :2:MD.size()];
                        atomSet2 = [i+1  :2:MD.size()];
                        if (mod(i,2)-mod(ConstraintAtomType,2))==0 % i and ConstraintAtomType are both odd or even 
                            BDM.atoms = atomSet1;
                        else 
                            BDM.atoms = atomSet2;
                        end
                    end
                    break;
                end
            end
            for i=1:FE.size()
                if(FE.X(i)>=RightBoundary)
                    BDM.nodes = [1:i];
                    break;
                end
            end
            BDM.real_l_bri = FE.dx * (i-1);
            BDM.weights = BDM.calculate_weights(MD,FE,bdm_overlap);
            BDM.inv_mass = BDM.calculate_masses(MD,FE);
            BDM.consistent = BDM_consistent;
        end

        % Calculate the corrective BDM forces.
        function f = forces(BDM,MD,FE,dt)
            g_star = BDM.calculate_gstar(MD,FE);
            lambda = BDM.calculate_lagrange_multipliers(MD,FE,g_star,dt);

            f.MD = zeros(MD.size(),1);
            f.MD(BDM.atoms) = lambda .* g_star.MD;
            f.FE = zeros(FE.size(),1);
            f.FE(BDM.nodes) = lambda' * g_star.FE;
        end
    end
    methods (Access = private)
        % Calculate the BDM weights.
        function weights = calculate_weights(BDM,MD,FE,bdm_overlap)
            weights.MD = ones(MD.size(),1);
            weights.FE = ones(FE.size(),1);
            LeftBoundary = MD.X(end)-bdm_overlap;
            %RightBoundary =node.X(1)+BDM.real_l_bri;

            weights.MD(BDM.atoms) = 1.0 - (MD.X(BDM.atoms) - LeftBoundary)/BDM.real_l_bri;
            weights.FE(BDM.nodes) =       (FE.X(BDM.nodes) - LeftBoundary)/BDM.real_l_bri;
            % Don't allow zero weights.
            weights.MD(weights.MD<1.0e-8) = 1.0e-4;
            weights.FE(weights.FE<1.0e-8) = 1.0e-4;
        end

        % Calculate the weighted masses.
        function inv_mass = calculate_masses(BDM,MD,FE)
            inv_mass.FE = 1.0./(BDM.weights.FE .* FE.mass);
            inv_mass.MD = 1.0./(BDM.weights.MD .* MD.mass);
        end

        % Calculate the lagrange multipliers.
        function lambda = calculate_lagrange_multipliers(BDM,MD,FE,g_star,dt)
            inv_mass_MD = BDM.inv_mass.MD(BDM.atoms);
            inv_mass_FE = diag(BDM.inv_mass.FE(BDM.nodes));

            A = g_star.FE * inv_mass_FE * g_star.FE' - diag(inv_mass_MD.*g_star.MD);
            g_star = g_star.FE * FE.v(BDM.nodes)-(MD.v(BDM.atoms));
            
            % Calculate the consistent or diagonalized constraint matrix.
            % Matrix is diagonalized using row-sum.
            if(BDM.consistent) lambda = A\g_star;
            else               lambda = g_star./sum(A,2);
            end
            lambda = lambda ./ dt;
        end

        % Calculate g_star.
        function g_star = calculate_gstar(BDM,MD,FE)
            g_star.MD = -ones(length(BDM.atoms),1);
            g_star.FE = zeros(length(BDM.atoms),length(BDM.nodes));

            for i = 1:length(BDM.atoms)     % index from the second atom  
                x = MD.X(BDM.atoms(i));
                for elem = FE.conn(:,BDM.nodes(1):BDM.nodes(end)-1)  % node index from the begin of continum mesh to the very end
                    if x< FE.X(elem(2)) && x>=FE.X(elem(1))
                        node_left = FE.X(elem(1));
                        node_right= FE.X(elem(2));
                        xita  = -1+(x-node_left)/(node_right-node_left)*2;
                        N = FE.shape_functions(xita);
                        g_star.FE(i,elem) =N; 
                    elseif( x == FE.X(BDM.nodes(end)))  % % x = elem(end)
                        xita=1;
                        N = FE.shape_functions(xita);
                        g_star.FE(i,elem) = N;
                        
                    end 
                end
            end
        end
    end
end

