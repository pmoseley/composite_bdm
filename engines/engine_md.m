classdef engine_md
    properties
        dx
        mass
        X
        x
        v
        a0
        a1
        KE
        PE
        e_total
        bondType
    end
    properties (Hidden)
        na
        conn
        potential
        
    end
    methods
        % Constructor.
        function MD = engine_md(na, m, potential, time)

            MD.potential = potential;
            MD.dx = MD.calculate_bond_len();        % size(MD.dx) represents the number of bonds.
            MD.na = na;
            MD.mass = MD.calculate_m(m);            % size(MD.mass) represents how many atoms are used
                                                    % can only deal with two types of atoms
            MD.X  = MD.calculate_X();
            MD.x  = zeros(na,1);
            MD.v  = zeros(na,1);
            MD.a0 = zeros(na,1);
            MD.a1 = zeros(na,1);
            MD.conn = [1:na-1; 2:na];
            MD.bondType = MD.calculate_bondType(); % bondType is [1 1 1 1... ]
                                                   % or [1 2 1 2 1 2 ..] as
                                                   % flag of bond types
            MD.KE = zeros(length(time),1);
            MD.PE = zeros(length(time),1);
            MD.e_total = zeros(length(time),1);
        end

        % Return the number of atoms.
        function na = size(MD)
            na = MD.na;
        end
        % Return the length of the domain.
        function len = length(MD)
            if (size(MD.dx)==[1 2])
                len = (MD.na-1)/2*sum(MD.dx);
            else
                len = (MD.na-1)*MD.dx;
            end
        end
        % Return the density. Assumes all atoms have same mass.
        function rho = density(MD)
            rho = sum(MD.mass)/MD.length();
        end

        % Calculate the MD forces.
        function f = forces(MD,alpha,LeftBC)
            % BondType is 1 or 2
            % alpha stuff still needs to be changed.
          
            f.ext = zeros(MD.na,1);
            f.int = zeros(MD.na,1);
            % Calculate forces on each atom pair.
            for e = MD.conn
                de = MD.x(e);
                BondType= MD.bondType(e(1));    % the bondType of this element
                re = de(2) + MD.dx(BondType) - de(1);
                force  = MD.atomic_potential(re);
                fe = [force(BondType); -force(BondType)];
                f.int(e) = f.int(e) + fe;
            end
            % Apply the BDM weights.
            f.int = alpha.*f.int;
            if (LeftBC)
                f.int(1) = 0;
            end
        end

        % Lennard-Jones inter-atomic potential.
        function f = atomic_potential(MD,r)
            % f could be 1 or 1*2 array
            e = MD.potential.epsilon;
            s = MD.potential.sigma;
            f = 48.0*e.*(s.^12./r.^13) - 24.0*e.*(s.^6./r.^7);
        end

        % Calculate the first PK stress using the Cauchy-Born rule.
        % Only works for 1d problems.
        function P = calculate_PK1(MD,F)
            sr0 = MD.potential.sigma ./ MD.dx;
            er0 = MD.potential.epsilon ./ MD.dx;
            P_temp = (24.0.*er0) .* (sr0.^6./F^7 - 2.0*sr0.^12./F.^13);
            P = P_temp(1);
        end
    end
    methods (Access = private)
        % Calculate the equilibrium bond length.
        function dx = calculate_bond_len(MD)
            e = MD.potential.epsilon; % remember dx could [1,2] array
            s = MD.potential.sigma;
            dx = (2.0*s.^6).^(1/6);
        end
        function mass = calculate_m(MD,m)
            % m could be a array. 
            na = MD.na;

            if (size(m)==[1,1])
                mass = m*ones(na,1);
            elseif (size(m)==[1,2])
                if (mod(na,2)~=1)
                    error('atom_mass has to be odd');
                end                
                mass = zeros(na,1)  ;
                mass(1:2:na)  = m(1);
                mass(2:2:na)  = m(2);
            end
        end
        function X = calculate_X(MD)
            na = MD.na;
            dx = MD.dx;
            if (size(dx)==[1,1])
                X = [0:MD.dx:MD.dx*(na-1)];
            elseif size(dx)==[1,2]
                X = zeros(na,1);
                X(1:2:end)= [0:sum(dx):sum(dx)*(na-1)/2];
                X(2:2:end)= [dx(1):sum(dx):sum(dx)*(na-1)/2];
            end
        end
        function bondType = calculate_bondType(MD)
            bondType = ones(MD.na,1);
            if size(MD.dx)==[1,2]           % according to dimension of dx
                                            % code knows how many types of
                                            % bonds are used. Maybe a little silly. 
                bondType(2:2:end) = 2;
            end
            
        end
    end
end

