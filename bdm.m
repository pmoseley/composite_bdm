function bdm(varargin)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Author:   Philip Moseley
    %               Philip.Moseley@u.northwestern.edu
    %
    % License:  Academic Free License v3.0. You may modify and distribute
    %           the code as you please, as long as you maintain the
    %           attribution notices.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    addpath(genpath('.'));

    %------------------------Input--------------------------
    dt      = 0.0005;           % Fine Timestep (ps).
    N       = 4;                % ratio between coarse/fine timestep.
    t_final = 1;                % Total simulation time.
    io_step = 20;               % Timesteps between plots.
    print_times = ...           % List of times to print.
        [0:t_final/2.0:t_final];
    output_file = 'output';     % Name of output files.

    num_atoms = 101;            % Number of atoms, has to be odd to have a even element number.
    num_nodes = 15;             % Number of nodes.
    bdm_overlap = 70.0;         % Overlapping domain length (Angstroms).

    dx_node =  10.0;            % Nodal spacing (Angstroms).
    atom_mass =  1.036e-4;      % Mass per atom. (eV*ps^2/A^2)
    potential.sigma = [1.1];    % Sigma for single potential (Angstroms).
    potential.epsilon = [0.1];  % Epsilon for each potential (eV).
    % potential.sigma = [1.1, 1.1];    % Sigmas for composite lattices (Angstroms).
    % potential.epsilon = [0.1, 0.1];  % Epsilons for composite lattices (eV).
    
    ConstraintAtomType = 0;     % 1 --> Constraint is applied on atom type 1.
                                % 2 --> Constraint is applied on atom type 2.
                                % 0 --> Constraint is applied on all atoms.
                                
    BDM_consistent = false;     % Consistent or diagonalized constraint matrix.

    % Initial conditions.
    T =  0.4;                   % Set the cycle time  .
    magnitude     = 0.005;      % This is the magnitude of initial wave shape.
    LeftBC        = true;
    RightBC       = false;
    LeftBoundary  = @(t) OneCycleSinFunc(magnitude,t,T);
    RightBoundary = @(t) 0;   


    
    % Load user input file, if specified.
    if(size(varargin,2)==1)
        names = fieldnames(varargin{1});
        for i = 1:length(names)
            eval([names{i}, ' = varargin{1}.(names{i});']);
        end
    end
    
    %----------------------Check Data------------------------
    if(size(potential.sigma)==[1 1])
        ConstraintAtomType = 0; % If there is only one kind of potential,
                                % constraint is applied on all atoms automatically
    % else
    %     if (ConstraintAtomType == 0)
    %         error('There are two kinds of potentials, "ConstraintAtomType" must be assigned to 1 or 2');
    %     end
    end
    

    %----------------------Initialize------------------------
    tic;                        % Start the clock.
    time = 0.0:dt:t_final;
    MD  = engine_md(num_atoms, atom_mass, potential, time);
    FE  = engine_fe(MD, num_nodes, dx_node, MD.length()-bdm_overlap, time);
    BDM = engine_bdm(MD, FE, bdm_overlap, BDM_consistent,ConstraintAtomType);

    % Plot the initial conditions.
    figure(); h.u = gca;
    figure(); h.e = gca;
    plot_results(h,MD,FE,'auto',time,1);
    bounds = axis(h.u); 
    bounds(4) = 2*magnitude; bounds(3) = -bounds(4); axis(h.u,bounds); % change bound, in order to see reflection
    pause(0.1);


    %----------------------Solve------------------------
    for i = 1:length(time)
        % Calculate forces on the MD at every fine timestep.
        force_MD = MD.forces(BDM.weights.MD,LeftBC);
        MD.a1 = (force_MD.ext-force_MD.int) .* BDM.inv_mass.MD;
        
        % Calculate forces on the FE at every coarse timestep.
        if(mod(i,N)==1)
            force_FE = FE.forces(BDM.weights.FE,RightBC);   
            FE.a1 = (force_FE.ext-force_FE.int) .* BDM.inv_mass.FE;
        end
        % Calculate the trial velocities.
        MD.v =  MD.v + 0.5*(MD.a0 + MD.a1)*dt;
        FE.v =  FE.v + 0.5*(FE.a0 + FE.a1)*dt;

        % Correct the velocities.
        v_corr = BDM.forces(MD,FE,dt);
        MD.v = MD.v - v_corr.MD.*BDM.inv_mass.MD*dt;
        FE.v = FE.v - v_corr.FE.*BDM.inv_mass.FE*dt;

        % Update the positions.
        MD.x = MD.x + MD.v*dt + 0.5*MD.a0*dt^2;
        FE.x = FE.x + FE.v*dt + 0.5*FE.a0*dt^2;
        if (LeftBC)  
            [LeftBC MD.x(1)]     = LeftBoundary(i*dt);  
        end
        if (RightBC) FE.x(FE.nn) = RightBoundary(i*dt); end
        
        MD.a0 = MD.a1;
        if(mod(i,N)==1)
            FE.a0 = FE.a1;
        end
        
        % Calculate Energy 
        MD = calculate_energy(MD,BDM.weights.MD,force_MD,i);
        FE = calculate_energy(FE,BDM.weights.FE,force_FE,i);
        
        % Plot iterations.
        if(mod(i,io_step)==0) plot_results(h,MD,FE,bounds,time,i); end
        if(sum(print_times==time(i))>0)
            print_results(h,MD,FE,bounds,time,i,output_file);
        end
    end
    toc     % Stop the clock and report the elapsed time.
end





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Helper Functions.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate energy.
function C = calculate_energy(C,weights,force,i)
    C.KE(i) = sum(0.5*weights .* (C.mass.*C.v.*C.v)); % Kinetic energy.
    C.PE(i) = -sum((force.ext-force.int).*C.x);       % Potential energy.
    % Energy totals.
    C.e_total(i) = C.KE(i) + C.PE(i);
end

% Plot the results.
function plot_results(h,MD,FE,bounds,time,i)
    % Displacement.
    plot(h.u,MD.X,MD.x,'.',FE.X,FE.x,'-ro');
    grid(h.u,'on'); axis(h.u,bounds);
    xlabel(h.u,'x (Angstroms)'); ylabel(h.u,'Displacement (Angstroms)');
    title(h.u,sprintf('Time (ps): %f',time(i)));

    % Energy.
    sys_energy = MD.e_total + FE.e_total;
    plot(h.e,time,MD.e_total,'-',time,FE.e_total,'r-',time,sys_energy);
    xlabel(h.e,'Time (ps)'); ylabel(h.e,'Energy (eV)')
    legend(h.e,'Total MD Energy','Total FE Energy','System Energy','Location','NorthWest');
    legend(h.e,'boxoff');
    grid(h.e,'on');

    pause(0.08);    % it seems that "pause time" can't be any shorter. 
end


% Print result figures to a file.
function print_results(h,MD,FE,bounds,time,i,file)
    plot_results(h,MD,FE,bounds,time,i);
    file = strcat(file,'-',num2str(i,'%07u'));
    % Encapsulated postscript.
    print('-f1','-depsc2', strcat(file,'-u.eps'));
    print('-f2','-depsc2', strcat(file,'-e.eps')); % may only print the last one. 

    % Uncomment this to use .png instead.
    % print('-f1','-dpng', strcat(file,'-u.png'));
    % print('-f2','-dpng', strcat(file,'-e.png'));
end


% Simple sample input function.
function [LeftBC y]=OneCycleSinFunc(magnitude,t,T)
    if (t<T/2)
        LeftBC = true;
        y=magnitude*sin(t*2*pi/T);
    else
        y=0;
        LeftBC = false;
    end
end
